package com;

import java.util.Random;

/**
 * Created by Евгений on 07.07.2017.
 */

public abstract class Realization implements SeaBattleInterface{

    int SIZE = 10;
    public  Random random = new Random();
    int[][]arrSea = new int [SIZE][SIZE];

    public void randomArrSea(){
        for(int i = 0; i  < SIZE; i++){
            for (int j=0; j < SIZE; j++){
                arrSea[i][j]=random.nextInt(2);
            }
        }

    }

    public void drawRandom(){

        for (int i = 0; i < SIZE; i++) {
            System.out.println("");
            for (int j = 0; j < SIZE; j++) {
                switch (arrSea[i][j]) {
                    case 0:
                        System.out.print("-");
                        break;
                    case 1:
                        System.out.print("X");
                        break;
                    default:
                        System.out.print("Somewhere I blunted!:(");
                }
            }
        }
        System.out.println("");
    }
}
